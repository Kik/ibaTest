<%-- 
    Document   : addStudent
    Created on : 20.6.2016, 9:27:10
    Author     : Kristina Miklasova, tina.miklasova@gmail.com
--%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <style>
        .error {
                color: #ff0000;
        }

        .errorblock {
                color: #000;
                background-color: #ffEEEE;
                border: 3px solid #ff0000;
                padding: 8px;
                margin: 16px;
        }
        </style>
    </head>
    <body>
        <h2>Add new student:</h2>
        <table>
            <form:form action="student" method="post" commandName="student">
                <form:errors path="*" cssClass="errorblock" element="div" />
                <tr>  
                    <td> <b> Name: </b> </td> 
                    <td> <form:input  path="name"/> </td> 
                    <td><form:errors path="name" cssClass="error" /></td>                 
                </tr> 
                <tr> 
                    <td> <b> Surname: </b> </td> 
                    <td><form:input path="surname"/> </td> 
                    <td><form:errors path="surname" cssClass="error" /></td>
                </tr> 
                <tr>
                    <td> <b> Birthday (dd.MM.yyyy): </b> </td>
                    <fmt:formatDate type="date" pattern="dd.MM.yyyy" var="theFormattedDate" />
                    <td><form:input type="text" path="birth" value="${theFormattedDate}"/></td>   
                    <td><form:errors path="birth" cssClass="error" /></td>
                </tr>
                <tr> 
                    <td> <b> Sex: </b> </td> 
                    <td><form:select path="sex" items="${sexList}" /></td> 
                </tr> 
                <tr> 
                    <td colspan=2> <input type="submit" value="Send"> </td>
                </tr>
         </form:form>
        </table>
    </body>
</html>

