<%-- 
    Document   : showStudent
    Created on : 20.6.2016, 9:27:54
    Author     : Kristina Miklasova, tina.miklasova@gmail.com
--%>

<html>
    <body>  
        <h2>Data about new student: </h2>
        <tr>
            <p> <td> <b>Name: </b> ${name} </td>  </p>   
            <p> <td> <b>Surname: </b> ${surname} </td> </p>
            <p> <td> <b>Date of birth: </b> ${birth} </td> </p>
            <p> <td> <b>Sex: </b> ${sex} </td> </p>
        </tr>
    </body>
</html>

