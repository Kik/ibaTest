<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <body>
        
        <c:if test="${not empty msg}">
            <c:forEach begin="1" end="${msg}">
                <p>Hello IBA !</p>
            </c:forEach>
        </c:if>
        <c:if test="${empty msg}">
            <p>Hello IBA !</p>
        </c:if>

    </body>
</html>
