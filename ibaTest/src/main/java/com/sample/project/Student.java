package com.sample.project;

import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * Entity of student with 4 parameters - name, surname, date of birth and sex. 
 * @author Kristina Miklasova, tina.miklasova@gmail.com
 */
public class Student {
    
    @NotNull(message = "Name is compulsory !")
    @Size(min=1, max=60, message = "Size of name must be between 1 and 60 characters !")
    private String name;  
    
    @NotNull(message = "Surname is compulsory !")
    @Size(min=1, max=60, message = "Size of surname must be between 1 and 60 characters !")
    private String surname;
    
    @NotNull(message = "Date of birth is compulsory !")
    @Past(message = "Date of birth must not be in the future !")
    @DateTimeFormat(pattern="dd.MM.yyyy")
    private Date birth;
    private Sex sex;

    public Student() { 
    }
    
    public Student(String name, String surname, Date birth, Sex sex) {
        this.name = name;
        this.surname = surname;
        this.birth = birth;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", surname=" + surname + ", date of birth=" + birth + ", sex=" + sex + '}';
    }
}

