package com.sample.project;

/**
 * Enum type for sex. 
 * @author Kristina Miklasova, tina.miklasova@gmail.com
 */
public enum Sex {
    MALE, FEMALE
}