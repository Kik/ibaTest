package com.sample.project.web.controller;

import com.sample.project.Sex;
import com.sample.project.Student;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Shows form for data about student (name, surname, date of birth and sex) and subsequently prints sent data.
 * Format of date of birth is dd.MM.yyyy
 * 
 * @author Kristina Miklasova, tina.miklasova@gmail.com
 */
@Controller
public class HelloController {
          
    private final List<Sex> SEX_LIST = new ArrayList<>(Arrays.asList(Sex.FEMALE, Sex.MALE));
    private final String ADD_JSP = "addStudent";
    private final String SHOW_JSP = "showStudent";
    
    /**
     * Creates model for printing form about student.
     * Sex is defined as enum type - female or male.
     * @return model for input form
     */
    @RequestMapping(value="add", method = RequestMethod.GET)
    public ModelAndView student(){       
        ModelAndView model = new ModelAndView(ADD_JSP, "student", new Student());
        model.addObject("sexList", SEX_LIST);
        
        return model;
    }
    
    /**
     * Loads sent data about student and prints it.
     * @param student entity about who data is sent
     * @param bindingResult
     * @param model model for entering data
     * @return showStudent.jsp for printing sent data about student
     */
    @RequestMapping(value="student", method = RequestMethod.POST)
    public String saveStudent(@Valid @ModelAttribute("student") Student student, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("sexList", SEX_LIST);
            return ADD_JSP;
        } 
        
        model.addAttribute("name", student.getName());
        model.addAttribute("surname", student.getSurname());
                
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        String testDateString = df.format(student.getBirth());        
        model.addAttribute("birth", testDateString);
        
        if (student.getSex().equals(Sex.FEMALE)) {
            model.addAttribute("sex", "female");
        } else {
            model.addAttribute("sex", "male");
        }
        return SHOW_JSP;
    }	
    
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }    
    
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages");
        return messageSource;
    }
}
